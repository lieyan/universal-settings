#!/usr/bin/env bash

# Functionality: Install common Pypi packages.

# Upgrade pip.
pip install --upgrade pip

# Install python packages.
pip install nsplist      #  A parser for NeXTSTEP-style plists.
