#!/usr/bin/env bash

# Functionality: Install common packages from Homebrew.

set -e

# Given a command name, check whether the command exists.
command_exists()
{
  command -v "$1" >/dev/null 2>&1
}

# Install the Rust toolchain.
install_rust() {
  brew install rustup     # The Rust toolchain installer
  rustup-init
  source $HOME/.cargo/env
}

# Install Python 3.6. Specific version: 3.6.5
install_python36() {
  brew install --ignore-dependencies \
  https://raw.githubusercontent.com/Homebrew/homebrew-core/f2a764ef944b1080be64bd88dca9a1d80130c558/Formula/python.rb

  pip3 install --upgrade pip
}


# Check whether command brew is available. If not, exit with non-zero.
[[ ! $(command_exists brew) ]] || {
  echo "Command brew doesn't exist. Please install it before go ahead."
  exit 1
}

# Update formula.
brew update

# Install common packages (quietly).
brew install ascii        # List ASCII idiomatic names and octal/decimal code-point forms
brew install boost        # Collection of portable C++ source libraries
brew install cmake        # Cross-platform make
brew install coreutils    # GNU File, Shell, and Text utilities
brew install fmt          # Open-source formatting library for C++
brew install ghostscript  # Interpreter for PostScript and PDF
brew install libressl     # Version of the SSL/TLS protocol forked from OpenSSL
brew install llvm         # Next-gen compiler infrastructure
brew install openssl      # SSL/TLS cryptography library
brew install python3      # Interpreted, interactive, object-oriented programming language
brew install sloccount    # Count lines of code in many languages
brew install tree         # Display directories as trees (with optional color/HTML output)

# Install common packages whose installation step is non-standard.
install_python36
install_rust