#!/usr/bin/env bash

# Functionality: Uninstall the Homebrew installation.

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall)"
