#!/usr/bin/env bash

# Functionality: Install common applications from Homebrew cask.

set -e

# Given a command name, check whether the command exists.
command_exists()
{
  command -v "$1" >/dev/null 2>&1
}

# Check whether command brew is available. If not, exit with non-zero.
[[ ! $(command_exists brew) ]] || {
  echo "Command brew doesn't exist. Please install it before go ahead."
  exit 1
}

# Update formula.
brew update

# Install common applications.
brew cask install go2shell          # https://zipzapmac.com/go2shell
brew cask install jetbrains-toolbox # https://www.jetbrains.com/toolbox/app/
