#!/usr/bin/env bash

# Functionality: Set up Homebrew (core as well as cask).

set -e

# Given a command name, check whether the command exists.
command_exists()
{
  command -v "$1" >/dev/null 2>&1
}

# Set up the brew repo with the tsinghua mirror.
setup_formula_repo_with_tsinghua_mirror()
{
  git -C "$(brew --repo)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/brew.git
  git -C "$(brew --repo homebrew/core)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-core.git
  git -C "$(brew --repo homebrew/cask)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-cask.git
  brew update
}

# Set up the brew repo with the official mirror.
setup_formula_repo_with_official_mirror()
{
  git -C "$(brew --repo)" remote set-url origin https://github.com/Homebrew/brew.git
  git -C "$(brew --repo homebrew/core)" remote set-url origin https://github.com/Homebrew/homebrew-core.git
  git -C "$(brew --repo homebrew/cask)" remote set-url origin https://github.com/Homebrew/homebrew-cask.git
  brew update
}

# Set up the bottles repo with the tsinghua mirror.
setup_bottles_repo_with_tsinghua_mirror()
{
  echo 'export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.tuna.tsinghua.edu.cn/homebrew-bottles' >> ~/.bash_profile
  source ~/.bash_profile
}

# If brew exits, exit with non-zero. Otherwise, install homebrew.
if command_exists brew; then
  echo "Command brew already exists."
  exit 1
else
  # Set up mirror for bottles.
  setup_bottles_repo_with_tsinghua_mirror
  # Set up workaround for curl.
  echo 'export HOMEBREW_FORCE_BREWED_CURL=1' >> ~/.bash_profile
  # Install homebrew and homebrew/cask.
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  brew tap homebrew/cask
  # Set up mirror for formula.
  setup_formula_repo_with_tsinghua_mirror
fi
